﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AminoCompare
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string FilePath { get; set; }
        public List<Protein> Document { get; set; }

        public int NumChars { get; set; }
        public bool FixedNum { get; set; }

        public List<Result> Data { get; set; }
        public List<Result> DataOrdered { get { return Data.Where(r => r.Number > 1).OrderByDescending(r => r.Number).ToList(); } }


        public MainWindow()
        {
            InitializeComponent();
            FilePath = null;
            Document = new List<Protein>();
            FixedNum = true;
        }

        private void LoadFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                FilePath = openFileDialog.FileName;
            }
            tb_Path.Text = FilePath;

            if (FilePath != null)
            {
                string text = File.ReadAllText(FilePath);

                string[] stringSeparators = new string[] { "\r\n", "\n", "\r" };
                string[] lines = text.Split(stringSeparators, StringSplitOptions.None);

                foreach (string line in lines)
                {
                    if (!line.Equals(""))
                    {
                        string[] protein = line.Split(';');
                        Document.Add(new Protein(protein[1], protein[0]));
                    }
                }
            }
        }

        private void Compare2(object sender, RoutedEventArgs e)
        {
            DateTime time = DateTime.Now;
            Data = new List<Result>();
            NumChars = Int32.Parse(tb_NumChar.Text);

            tb_Calc.Text = "Berechne...";
            if (NumChars > 0)
            {
                if (FixedNum)
                {
                    foreach (Protein protein in Document)
                    {
                        ScanLineFixedNum2(protein, 0, protein.Sequence.Length);

                        DataGridResult.ItemsSource = null;
                        DataGridResult.ItemsSource = Data;
                    }
                }

                DataGridResult.ItemsSource = null;
                DataGridResult.ItemsSource = DataOrdered;
            }
            tb_Calc.Text = "Fertig";
            double diff = (DateTime.Now - time).TotalMinutes;
            MessageBox.Show("Dauer: " + diff + "\n" + (diff * 50) + "Minuten mit 1500 Zeichenfolgen", "");

            string result = string.Join("\n", DataOrdered.Select(r => ProteinAsString(r)));
            string path = FilePath + "_Result_" + NumChars + ".csv";
            File.WriteAllText(path, result);

        }
        public delegate void UpdateTextCallback(string message);

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Data = new List<Result>();
            NumChars = Int32.Parse(tb_NumChar.Text);
            tb_Calc.Text = "Berechne...";
            
            Thread thread = new Thread(new ThreadStart(Compare)) { IsBackground = true };
            thread.Start();

            tb_Calc.Text = "Fertig";

            string result = string.Join("\n", DataOrdered.Select(r => ProteinAsString(r)));
            string path = FilePath + "_Result_" + NumChars + ".csv";
            File.WriteAllText(path, result);
        }

        private void Compare()
        {
            DateTime time = DateTime.Now;
            if (NumChars > 0)
            {
                if (FixedNum)
                {
                    int i = 0;
                    int j = Document.Count;
                    foreach (Protein protein in Document)
                    {
                        i++;
                        ScanLineFixedNum(protein, 0, protein.Sequence.Length);

                        tb_Calc.Dispatcher.Invoke(new UpdateTextCallback(UpdateText), new object[] { i + "/" + j });
                    }
                }
            }
            double diff = (DateTime.Now - time).TotalMinutes;
            //MessageBox.Show("Dauer: " + diff, "");

            this.Dispatcher.Invoke(() =>
            {
                DataGridResult.ItemsSource = null;
                DataGridResult.ItemsSource = DataOrdered;
            });
        }

        private void UpdateText(string message)
        {
            tb_Calc.Text = message;
        }

        private string ProteinAsString(Result result)
        {
            return result.Chars + ";" + result.Number;
        }

        private void ScanLineFixedNum(Protein protein, int idx, int length)
        {
            if (idx + NumChars < length)
            {
                string text = "";
                for (int i = 0; i < NumChars; i++)
                {
                    text += protein.Sequence[idx + i];
                }

                Result result = Data.Find(r => r.Chars.Equals(text));


                if (result != null)
                {
                    if (!result.ProteinList.Contains(protein.Name))
                    {
                        result.Number += 1;
                    }
                }
                else
                {
                    result = new Result(text, protein.Name);
                    Data.Add(result);
                }


                ScanLineFixedNum(protein, idx + 1, length);
            }
            else
            {
                Console.WriteLine(protein.Name);
                return;
            }
        }

        private void ScanLineFixedNum2(Protein protein, int idx, int length)
        {
            if (idx + NumChars < length)
            {
                string text = "";
                for (int i = 0; i < NumChars; i++)
                {
                    text += protein.Sequence[idx + i];
                }

                Result result = Data.Find(r => r.Chars.Equals(text));
                if (result != null)
                {
                    result.Number += 1;
                }
                else
                {
                    result = new Result(text, protein.Name);
                    Data.Add(result);
                }

                ScanLineFixedNum2(protein, idx + 1, length);
            }
            else
            {
                Console.WriteLine(protein.Name);
                return;
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox checkbox = (CheckBox)sender;
            if ((bool)checkbox.IsChecked)
            {
                tb_NumChar.IsEnabled = false;
                FixedNum = false;
            }
            else
            {
                tb_NumChar.IsEnabled = true;
                FixedNum = true;
            }
        }

    }

    public class Protein
    {
        public Protein(string name, string sequence)
        {
            this.Name = name;
            this.Sequence = sequence;
        }

        public string Name { get; set; }
        public string Sequence { get; set; }
    }
}
