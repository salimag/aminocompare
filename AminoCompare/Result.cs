﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AminoCompare
{
    public class Result
    {
        public Result(string chars, string proteinName)
        {
            Chars = chars;
            Number = 1;
            ProteinList = new List<string>();
            ProteinList.Add(proteinName);
        }

        public string Chars { get; set; }
        public int Number { get; set; }
        public List<string> ProteinList { get; set; }
    }
}